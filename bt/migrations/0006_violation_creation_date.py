# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('bt', '0005_auto_20160302_1202'),
    ]

    operations = [
        migrations.AddField(
            model_name='violation',
            name='creation_date',
            field=models.DateField(default=datetime.datetime(2016, 3, 2, 12, 14, 23, 907012), auto_now_add=True),
            preserve_default=False,
        ),
    ]
