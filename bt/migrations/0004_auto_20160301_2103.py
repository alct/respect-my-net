# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('bt', '0003_auto_20160301_2032'),
    ]

    operations = [
        migrations.AlterField(
            model_name='violation',
            name='activationid',
            field=models.CharField(max_length=128, null=True, blank=True),
        ),
    ]
